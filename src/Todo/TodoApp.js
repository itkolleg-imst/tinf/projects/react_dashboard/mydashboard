import { useState } from 'react';
import AddButton from './AddButton';
import Itemslist from './Itemslist';
import './TodoApp.css';

function TodoApp() {
  const itemslist = [
    { id: 1, task: 'A task that needs to be done within a certain period of time.', taskstatus:false },
    { id: 2, task: 'Another task that needs to be done within a certain period of time.', taskstatus:true },
  ];
  //The State of our app: all todos live here
  const [todos, setTodos] = useState(itemslist);

  function handleAddButtonClick() {
    const todotext = alert('Please enter a new ToDo');
    setTodos([...todos, { id: todos.length + 1, task: todotext, taskstatus: false}]); 
  }

  function handleCheckBoxChange(id) {
    // alert('You clicked me:'+id);
    setTodos(todos.map(todo => {
      if (todo.id !== id) {
        return {...todo, taskstatus: !todo.taskstatus};
      }
      return todo;
    }));
  }

  return (
    <div className="todo">
      <h2>My ToDos</h2>
      <AddButton todos={todos} onButtonClick={handleAddButtonClick}/>
      <ul>
        <Itemslist itemslist={todos} onCheckBoxChange={handleCheckBoxChange}/>
      </ul>
    </div>
  );
}

export default TodoApp;
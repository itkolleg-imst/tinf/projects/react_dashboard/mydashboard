import { useState } from "react";

function AddButton({ value, onButtonClick }) {

    return (
        <button title='Adds a new ToDo' onClick={onButtonClick}>+</button>
    );
}

export default AddButton;